GEOSTAT
=======

Tools for computing various statistics related to Geocaching/Opencaching
------------------------------------------------------------------------

# Sample web interface

A sample web interface to play with the tool is available at http://tomaszg.pl/geo/.


# Usage

The main tool is called `geostat`. It is able to access `opencaching.pl` via `OKAPI` or from local database dump maintained by `geodb`. It can also partially read `GPX` files, e.g. exported from [c:geo](https://www.cgeo.org/). It produces various statistics (either as text output with `geostat_cli`, or in a HTML form with `geostat`) and renders a heat map of found/not found caches in a selected region. HTML output requires a style sheet. Sample one is included as `geo.css`.

```
Usage: geostat [options]
Generate HTML stats from Opencaching data or GPX files.

 * Data sources:
	-o[user]	use Opencaching; if specified user will be a user name used for all OC instances
	-p user		user for opencaching.pl
	-d user		user for opencaching.de
	-u user		user for opencaching.us
	-n user		user for opencaching.nl
	-r user		user for opencaching.ro
	-k user		user for opencaching.uk
	-q		use local SQLite file with dump of OC database
	-i timestamp	start date
	-f timestamp	finish date

 * Output:
	-N		compute stats only for unfound caches (works only with SQLite)
	-O		compute stats only for owned caches (works only with SQLite)
	-Q		exclude quiz caches from unfound caches
	-H file		render a heat map to a file
	-s n		stamp size for a heat map (default = 15)
	-e		use exponential to flatten the heat map
	-t		draw trail instead of heat map
	-a		draw animated map instead of heat map
	-m map		chosen map: Poland, Poland_relief, Poland_big, Europe, World or a name of voivodeship (default = Poland)
	-h		display this help screen
```

# Other tools

## geodb
There is a helper program `geodb` which manages local SQLite database. It can initialize the database from full data dump or update it according to changelogs grabbed from OKAPI.

```
Usage: geodb [-i path] [-uh]
Manage local Opencaching database.
	-i folder	initialize database from dump located in given folder
	-u	update database with changelogs grabbed via OKAPI
	-h	display this help screen
```

## geofriends
Another tools is `geofriends`. It generates stats for a pair of users regarding their common caching activities.

```
Usage: geofriends nick1 nick2
Generate HTML stats for user caching intersection from Opencaching data.
```

## geolist
Another tools is `geolist`. It generates HTML list of caches from a list of cache codes.

```
Usage: geolist list_of_caches
Produce a HTML list of caches given their OC codes.
```

# Installation

To compile it requires:

*   C++ compliler supporting C++17 and a minor subset of C++20 (gcc >=9 or clang =>8 should be enough).
*   meson (at least 0.47.0) + ninja build system
*   libcurl: https://curl.haxx.se/libcurl/
*   JSON for Modern C++: https://github.com/nlohmann/json
*   gpxlib: http://irdvo.nl/gpxlib/
*   heatmap: https://github.com/lucasb-eyer/heatmap
*   ImageMagick or GraphicsMagick Magick++ library

To build the project copy `config_user_example.h` to `config_user.h` and fill it out (at least API key is needed). Then
run 

```
meson build
cd build
ninja
ninja install
```

You might need to set `CXX` variable to point to a correct compiler.

# Credits

* Maps of Poland `Poland.png` and `Poland_big.png` come from https://pl.wikipedia.org/wiki/Plik:Poland_location_map.svg and are licensed under CC-BY-SA and GNU FDL.

* Map of Poland `Poland_relief.png` comes from https://commons.wikimedia.org/wiki/File:Relief_Map_of_Poland.svg and is licensed under CC-BY-SA.

* Map of Poland `Poland_powiaty.png` comes from https://en.wikipedia.org/wiki/File:POLSKA_powiaty.svg and is licensed under CC-BY-SA.

* Map of France `Benelux.png` comes from https://pl.wikipedia.org/wiki/Plik:Benelux_location_map.svg and is licensed under CC-BY-SA.

* Map of France `France.png` comes from https://commons.wikimedia.org/wiki/File:France_location_map.svg and is licensed under CC-BY-SA and GNU FDL.

* Map of Germany `Germany.png` comes from https://commons.wikimedia.org/wiki/File:Germany_location_map.svg and is licensed under CC-BY-SA and GNU FDL.

* Map of Romania `Romania.png` comes from https://en.wikipedia.org/wiki/File:Romania_location_map.svg and is licensed under CC-BY-SA and GNU FDL.

* Map of UK `UK.png` comes from https://commons.wikimedia.org/wiki/File:United_Kingdom_NUTS_location_map.svg and is licensed under CC-BY-SA.

* Map of USA `USA.png` comes from https://commons.wikimedia.org/wiki/File:USA_location_map.svg and is licensed under CC-BY-SA and GNU FDL.

* Map of Europe `Europe.png` comes from https://pl.wikipedia.org/wiki/Plik:Europe_location_map.svg and is licensed under PD.

* Map of the World `World.png` comes from https://commons.wikimedia.org/wiki/File:World_location_map.svg and is licensed under PD.

* Maps of Voivodeships of Poland come from https://commons.wikimedia.org/wiki/File:Podlaskie_Voivodeship_location_map.svg, https://commons.wikimedia.org/wiki/File:Pomeranian_Voivodeship_location_map.svg, https://pl.wikipedia.org/wiki/Plik:Kuyavian-Pomeranian_Voivodeship_location_map.svg, https://pl.wikipedia.org/wiki/Plik:Warmian-Masurian_Voivodeship_location_map.svg, https://pl.wikipedia.org/wiki/Plik:Masovian_Voivodeship_location_map.svg, https://pl.wikipedia.org/wiki/Plik:Lesser_Poland_Voivodeship_location_map.svg, https://pl.wikipedia.org/wiki/Plik:Greater_Poland_Voivodeship_location_map.svg, https://pl.wikipedia.org/wiki/Plik:Opole_Voivodeship_location_map.svg, https://pl.wikipedia.org/wiki/Plik:Lower_Silesian_Voivodeship_location_map.svg, https://pl.wikipedia.org/wiki/Plik:Silesian_Voivodeship_location_map.svg, https://pl.wikipedia.org/wiki/Plik:Lublin_Voivodeship_location_map.svg, https://pl.wikipedia.org/wiki/Plik:Subcarpathian_Voivodeship_location_map.svg, https://pl.wikipedia.org/wiki/Plik:West_Pomeranian_Voivodeship_location_map.svg, https://pl.wikipedia.org/wiki/Plik:%C5%9Awi%C4%99tokrzyskie_Voivodeship_location_map.svg, https://pl.wikipedia.org/wiki/Plik:Lubusz_Voivodeship_location_map.svg, https://pl.wikipedia.org/wiki/Plik:%C5%81%C3%B3d%C5%BA_Voivodeship_location_map.svg and are licensed under CC-BY-SA.

* Data about administrative boundaries of Poland comes from OpenStreetMap via https://wambachers-osm.website/boundaries/.

* CSS tricks used to render histograms are based on https://css-tricks.com/making-charts-with-css.

* Website uses https://github.com/CaptainCodeman/gif-player to display gifs.
