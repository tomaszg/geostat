#pragma once

#include "api.h"

#include <string>
#include <set>
#include <utility>

typedef void CURL;

class Okapi : public Api {
private:
	std::string url;
	std::string key;

	Service service;

	CURL* curl;
	mutable std::string curl_output;

	static size_t write_cb(char* ptr, size_t size, size_t nmemb, void* userdata);
	void curl_post(const std::string& url, const std::string& post) const;
	void get_user_caches_json(const std::string& uuid, int count = 0, int offset = 0) const;
	// 	std::string get_cache_json(std::string code) const;
	void get_caches_json(const std::string& codes) const;
	// void get_caches_ratings_json(const std::string& codes) const;

	const static int MAX_LOGS = 1000;
	const static int MAX_CACHES = 500;

	constexpr static auto OKAPI_logs = "services/logs/userlogs";
	constexpr static auto OKAPI_cache = "services/caches/geocache";
	constexpr static auto OKAPI_caches = "services/caches/geocaches";
	constexpr static auto OKAPI_username = "services/users/by_username";
	constexpr static auto OKAPI_user = "services/users/user";
	constexpr static auto OKAPI_changelog = "services/replicate/changelog";

	constexpr static auto ocpl_url = "https://opencaching.pl/okapi/";
	constexpr static auto ocde_url = "https://www.opencaching.de/okapi/";
	constexpr static auto ocus_url = "http://www.opencaching.us/okapi/";
	constexpr static auto ocnl_url = "http://www.opencaching.nl/okapi/";
	constexpr static auto ocro_url = "http://www.opencaching.ro/okapi/";
	constexpr static auto ocuk_url = "https://opencache.uk/okapi/";

public:
	explicit Okapi(const Service serv);
	~Okapi();

	// 	Cache get_cache(std::string code);
	Caches get_caches(const std::set<std::string>& codes) const;
	Caches get_user_caches(const std::string& uuid, int count = 0) const override;
	int get_user_hidden_caches_no(const std::string& uuid) const;
	std::string get_changelog_json(int revision) const;

	void update_caches(Caches& cc) const;
	// 	void update_caches_ratings(Caches& cc) const;
	std::string get_uuid(const std::string& username, uint* id = nullptr) const;
	std::string get_profile_url(const std::string& uuid) const;
	void get_ftf(uint uid, Caches& cc) const;
};
