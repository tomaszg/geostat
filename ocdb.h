#pragma once

#include "cache.h"
#include "api.h"

#include <string>
#include <map>
#include <nlohmann/json_fwd.hpp>

typedef struct sqlite3 sqlite3;
typedef struct sqlite3_stmt sqlite3_stmt;
class Okapi;

using json = nlohmann::json;

class OCdb : public Api {
private:
	sqlite3* db;
	mutable sqlite3_stmt* stmt;
	mutable std::string sql;
	int revision;

	bool request(const std::string& req) const;
	bool init_part(const std::string& json_file); // read db dump
	bool parse_item(const json& j);
	bool update_cache(const json& j);
	bool update_log(const json& j);
	bool read_revision();
	Caches parse_sql_caches() const;

	enum LogType {
		found = 1,
		dnf = 0,
		other = -1
	};

	constexpr static auto Database_pl = "ocpl.sqlite";
	constexpr static auto Database_de = "ocde.sqlite";
	constexpr static auto Database_us = "ocus.sqlite";

public:
	explicit OCdb(const Service service);
	~OCdb();

	bool init(const std::string& dump_path); // read db dump
	bool update(const Okapi& oc); // apply changelog
	int get_revision() const;
	void set_revision(int rev);

	Caches get_user_caches_not_found(const std::string& uuid) const;
	Caches get_user_caches(const std::string& uuid, int count = 0) const override;
	Caches get_user_caches_owned(const std::string& uuid) const;
	std::map<std::string, int> get_region_stats();
};
