#pragma once

#include "common.h"
#include "cache.h"
#include "powertrail.h"
#include "region.h"

#include <map>

class User {
private:
	PowertrailDB traildb;
	Caches_in_Powertrails tc;

	std::string ocpl_user_profile;
	std::string ocde_user_profile;
	std::string ocus_user_profile;
	std::string ocnl_user_profile;
	std::string ocro_user_profile;
	std::string ocuk_user_profile;

public:
	// Position home;
	std::string ocpl_user_uuid;
	std::string ocde_user_uuid;
	std::string ocus_user_uuid;
	std::string ocnl_user_uuid;
	std::string ocro_user_uuid;
	std::string ocuk_user_uuid;

	bool use_oc = 0;
	bool use_ocdb = 0;
	std::string ocpl_user;
	std::string ocde_user;
	std::string ocus_user;
	std::string ocnl_user;
	std::string ocro_user;
	std::string ocuk_user;
	// 	std::string gpx_file;

	bool get_not_found = 0;
	bool get_owned = 0;
	bool exclude_quiz = 0;
	std::time_t start_time = 0;
	std::time_t end_time = std::time(nullptr);
	bool time_filter = 0;

	Caches cc;
	int caches_count = 0;
	int caches_hidden = 0;
	std::map<std::string, int> dates;
	Date_Caches sorted_caches;
	Date_Caches sorted_fcaches;
	Date_Caches sorted_caches_by_hidden;
	pCaches caches_by_fav;
	pCaches caches_by_fav_perc;
	pCaches caches_by_finds;
	pCaches caches_ftf;
	// pCaches caches_by_rating;
	pCaches fcc;
	pPowertrails tt;
	std::map<std::string, int> region_count;

	void get_caches();
	void prepare_lists_of_caches(); // Prepare sorted list of caches, excluding moving caches

	void header() const;

	std::string user_link(std::string name) const;
};
