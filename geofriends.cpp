#include "debug.h"
#include "user.h"
#include "common.h"

#include <iostream>
#include <algorithm>

void show_usage() {
	std::cout << "Usage: geofriends nick1 nick2\n";
	std::cout << "Generate HTML stats for user caching intersection from Opencaching data.\n\n";
	std::exit(EXIT_FAILURE);
}

int main(int argc, char** argv) {
	if (argc < 3) show_usage();

	User user1, user2;
	user1.ocpl_user = argv[1];
	user2.ocpl_user = argv[2];
	user1.use_oc = true;
	user2.use_oc = true;
	user1.get_caches();
	user2.get_caches();

	int cc_no1 = user1.caches_hidden;
	int cc_no2 = user2.caches_hidden;

	pCaches caches_by_user1, caches_by_user2;

	for (auto& i : user2.cc) {
		if (i.owner_uuid == user1.ocpl_user_uuid)
			caches_by_user1.push_back(&i);
	}

	for (auto& i : user1.cc) {
		if (i.owner_uuid == user2.ocpl_user_uuid)
			caches_by_user2.push_back(&i);
	}

	header_html();

	std::cout << "<header>\n";
	std::cout << "<h1><a href=\"/geo\">Geocaching stats</a> for user profiles:</h1>\n";
	user1.header();
	user2.header();
	std::cout << "</header>\n";

	std::cout << "<div class=\"users\">\n";
	std::cout << "<div class=\"user\">\n";

	// const int LIST_MAX = 100;
	short int n = 1;

	std::cout << "<h2>Caches created by " << user1.ocpl_user << " found by " << user2.ocpl_user << "</h2>\n";
	std::cout << "<div class=\"basic_stats\">\n";
	std::cout << "Number of caches found: <span class=\"value\">" << caches_by_user1.size() << "</span><br>\n";
	std::cout << "Number of recommendations given: <span class=\"value\">" << std::count_if(user2.cc.begin(), user2.cc.end(), [user1](const auto& c) { return (c.recommended && c.owner_uuid == user1.ocpl_user_uuid); }) << "</span><br>\n";
	std::cout << "</div>\n";
	if (cc_no1 > 0) {
		std::cout << "<div class=\"histogram friendbar\">\n";
		std::cout << "<div class=\"bar\" style=\"--percent: " << 100 * caches_by_user1.size() / cc_no1 << "%;\"><span class=\"text\">" << 100 * caches_by_user1.size() / cc_no1 << "%</span></div>\n";
		std::cout << "</div>\n";
		std::cout << "<table class=\"list\">\n";
		std::cout << "<tr><th></th>";
		std::cout << "<th>Cache</th>";
		std::cout << "<th>Type</th>";
		std::cout << "<th>Date hidden</th>";
		std::cout << "<th>Date found</th>";
		std::cout << "<th>Region</th>";
		std::cout << "</tr>\n";

		for (auto& i : caches_by_user1) {
			std::cout << "<tr><th>" << n << "</th> ";
			std::cout << "<td>" << i->link_name() << "</td>";
			std::cout << "<td>" << i->type << "</td>";
			std::cout << "<td>" << i->date_hidden << "</td>";
			std::cout << "<td>" << i->date << "</td>";
			std::cout << "<td>" << i->region << "</td>";
			std::cout << "</tr>\n";
			n++;
			// if (n > LIST_MAX) break;
		}
		std::cout << "</table>\n";
	}
	std::cout << "</div>\n";
	std::cout << "<div class=\"user\">\n";
	n = 1;
	std::cout << "<h2>Caches created by " << user2.ocpl_user << " found by " << user1.ocpl_user << "</h2>\n";
	std::cout << "<div class=\"basic_stats\">\n";
	std::cout << "Number of caches found: <span class=\"value\">" << caches_by_user2.size() << "</span><br>\n";
	std::cout << "Number of recommendations given: <span class=\"value\">" << std::count_if(user1.cc.begin(), user1.cc.end(), [user2](const auto& c) { return (c.recommended && c.owner_uuid == user2.ocpl_user_uuid); }) << "</span><br>\n";
	std::cout << "</div>\n";

	if (cc_no2 > 0) {
		std::cout << "<div class=\"histogram friendbar\">\n";
		std::cout << "<div class=\"bar\" style=\"--percent: " << 100 * caches_by_user2.size() / cc_no2 << "%;\"><span class=\"text\">" << 100 * caches_by_user2.size() / cc_no2 << "%</span></div>\n";
		std::cout << "</div>\n";
		std::cout << "<table class=\"list\">\n";
		std::cout << "<tr><th></th>";
		std::cout << "<th>Cache</th>";
		std::cout << "<th>Type</th>";
		std::cout << "<th>Date hidden</th>";
		std::cout << "<th>Date found</th>";
		std::cout << "<th>Region</th>";
		std::cout << "</tr>\n";

		for (auto& i : caches_by_user2) {
			std::cout << "<tr><th>" << n << "</th> ";
			std::cout << "<td>" << i->link_name() << "</td>";
			std::cout << "<td>" << i->type << "</td>";
			std::cout << "<td>" << i->date_hidden << "</td>";
			std::cout << "<td>" << i->date << "</td>";
			std::cout << "<td>" << i->region << "</td>";
			std::cout << "</tr>\n";
			n++;
			// if (n > LIST_MAX) break;
		}
	}
	std::cout << "</table>\n";
	std::cout << "</div>\n";
	std::cout << "</div>\n";

	footer_html();
}
