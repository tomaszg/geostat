#pragma once

#include <iostream>
#include <string>
#include <sstream>

class Debug {
private:
	int lvl; // debug level of the particular instance of Debug stream
	static int debug_level; // debug level set globally
	std::stringstream buffer;

public:
	explicit Debug(int n);
	~Debug();

	// Copy constructor and assignment operators are deleted to prevent extra destructor and constructor calls when chaining << operator
	Debug(const Debug&) = delete;
	Debug& operator=(const Debug&) = delete;

	template <typename T>
	Debug& operator<<(T const& value) {
		if (lvl <= debug_level)
			buffer << value;
		return *this;
	}
	static void set_debug_level(int n);
};
