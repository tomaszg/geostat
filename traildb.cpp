#include "powertrail.h"
#include "debug.h"

#include <fstream>

#include <nlohmann/json.hpp>

using json = nlohmann::json;

// void to_json(json& j, const Powertrail& item) {
//         j = json{ { "name", item.name }, { "date", item.date_str }, { "treshold", item.treshold_perc }, { "caches", item.caches } };
// }

int main() {
	// 	Debug::set_debug_level(5);
	PowertrailDB T;

	for (int i = 1; i < 2000; i++)
		T.get_trail(i);

	Caches_in_Powertrails cc(T);

	cc.save_to_json("caches_in_power.json.new");
	T.save_to_json("powertrails.json.new");
}
