#pragma once

#include <map>

class Map {
public:
	const int size_x;
	const int size_y;
	const float lat_max;
	const float lat_min;
	const float lon_max;
	const float lon_min;

	const std::string map_file;

	Map(int sx, int sy, float lat_mi, float lat_ma, float lon_mi, float lon_ma, const std::string& file) : size_x(sx), size_y(sy), lat_max(lat_ma), lat_min(lat_mi), lon_max(lon_ma), lon_min(lon_mi), map_file(file){};

	bool contains(Position pos) const {
		return (pos.lon >= lon_min && pos.lon <= lon_max && pos.lat >= lat_min && pos.lat <= lat_max);
	}
	int coordinate_x(Position pos) const {
		return (pos.lon - lon_min) / (lon_max - lon_min) * size_x;
	}
	int coordinate_y(Position pos) const {
		return size_y - static_cast<int>((pos.lat - lat_min) / (lat_max - lat_min) * size_y);
	}
};

const std::map<std::string, Map> maps = {
	{ "Poland", Map(1000, 972, 48.7, 55.2, 13.8, 24.5, "Poland.png") },
	{ "Poland_relief", Map(1000, 972, 48.7, 55.2, 13.8, 24.5, "Poland_relief.png") },
	{ "Poland_powiaty", Map(1406, 1339, 49.00238, 54.833333, 14.12298, 24.14585, "Poland_powiaty.png") },
	{ "Poland_big", Map(2560, 2488, 48.7, 55.2, 13.8, 24.5, "Poland_big.png") },
	{ "Podlaskie", Map(727, 1024, 52.17, 54.5, 21.45, 24.1, "Podlaskie.png") },
	{ "Pomorskie", Map(1000, 785, 53.40, 54.92, 16.65, 19.75, "Pomorskie.png") },
	{ "Kujawsko-Pomorskie", Map(1122, 1024, 52.28, 53.83, 17.16, 19.88, "Kujawsko-Pomorskie.png") },
	{ "Malopolskie", Map(1106, 1024, 49.07, 50.59, 18.92, 21.55, "Malopolskie.png") },
	{ "Mazowieckie", Map(1009, 1024, 50.95, 53.55, 19.15, 23.25, "Mazowieckie.png") },
	{ "Warminsko-Mazurskie", Map(1280, 762, 53.07, 54.52, 19.05, 22.95, "Warminsko-Mazurskie.png") },
	{ "Wielkopolskie", Map(827, 1024, 51.05, 53.70, 15.68, 19.19, "Wielkopolskie.png") },
	{ "Opolskie", Map(939, 1024, 49.942, 51.2778, 16.8461, 18.8073, "Opolskie.png") },
	{ "Dolnoslaskie", Map(1043, 1024, 49.9809, 51.9134, 14.7603, 17.9091, "Dolnoslaskie.png") },
	{ "Slaskie", Map(743, 1023, 49.2956, 51.1617, 17.8872, 20.0559, "Slaskie.png") },
	{ "Lubelskie", Map(813, 1024, 50.20, 52.35, 21.52, 24.25, "Lubelskie.png") },
	{ "Podkarpackie", Map(863, 1024, 48.95, 50.9, 21.03, 23.66, "Podkarpackie.png") },
	{ "Zachodniopomorskie", Map(974, 1024, 52.58, 54.65, 13.95, 17.10, "Zachodniopomorskie.png") },
	{ "Swietokrzyskie", Map(1181, 1024, 50.1, 51.4, 19.6, 22, "Swietokrzyskie.png") },
	{ "Lubuskie", Map(761, 1024, 51.33, 53.18, 14.4, 16.6, "Lubuskie.png") },
	{ "Lodzkie", Map(1073, 1024, 50.78, 52.45, 17.95, 20.75, "Lodzkie.png") },

	{ "Warszawa", Map(1065, 1063, 52.0933, 52.3732, 20.8283, 21.2846, "Warszawa.png") },
	{ "Krakow", Map(1280, 820, 49.95, 50.15, 19.76, 20.26, "Krakow.png") },
	{ "Trojmiasto", Map(1145, 1024, 54.2722, 54.5872, 18.3544, 18.9569, "Trojmiasto.png") },

	{ "Benelux", Map(960, 1304, 49.2, 53.7, 2.2, 7.5, "Benelux.png") },
	{ "France", Map(1041, 997, 41, 51.6, -5.3, 10.2, "France.png") },
	{ "Germany", Map(1073, 1272, 47.2, 55.1, 5.5, 15.5, "Germany.png") },
	{ "Romania", Map(1280, 915, 43.4, 48.5, 20, 30, "Romania.png") },
	{ "UK", Map(886, 1369, 49, 61, -11, 2.2, "UK.png") },
	{ "USA", Map(1280, 667, 24.2, 49.8, -125.5, -66.5, "USA.png") },

	{ "Europe", Map(1249, 1024, 28, 82, -25, 54, "Europe.png") },
	{ "World", Map(1280, 640, -90, 90, -180, 180, "World.png") },
};
