#pragma once

#include <string>
#include <unordered_set>
#include <unordered_map>
#include <vector>
#include <ctime>

#include "cache.h"

typedef void CURL;
class Caches_in_Powertrails;

enum Powertrail_Type {
	geoszkic,
	krajoznawcza,
	przyrodnicza,
	tematyczna,
	unknown_type
};

class Powertrail {
public:
	uint number;
	std::string name;
	Powertrail_Type type = unknown_type;
	std::tm date;
	std::string date_str;
	uint treshold_perc;
	uint treshold;
	std::unordered_set<std::string> caches;

	uint active_caches = 0;

	uint found = 0;
	uint completed_perc;
	bool completed = 0;
	uint needed = 0;

	uint active_caches_not_found = 0;

	std::string link() const;
	std::string link_name() const;
	std::string safe_name() const;
	std::string get_type() const;

	inline static const std::string types[] = { "geo-szkic", "krajoznawcza", "przyrodnicza", "tematyczna", "nieznany" };
};

class PowertrailDB {
public:
	std::unordered_map<uint, Powertrail> data;
	void get_trail(uint n);

	CURL* curl;
	std::string curl_output;

	PowertrailDB();
	~PowertrailDB();

	void read_from_json(std::string file);
	void save_to_json(std::string file);

private:
	static size_t write_cb(char* ptr, size_t size, size_t nmemb, void* userdata);
};

class Caches_in_Powertrails {
public:
	std::unordered_map<std::string, int> data;

	explicit Caches_in_Powertrails(const PowertrailDB& db);
	Caches_in_Powertrails(){};

	void read_from_json(std::string file);
	void save_to_json(std::string file);

private:
};

typedef std::vector<const Powertrail*> pPowertrails;
