#pragma once

#include "api.h"

#include <string>
#include <list>

namespace gpx {
class WPT;
class GPX;
} // namespace gpx

class GPX : public Api {
private:
	std::list<gpx::WPT*> items;
	gpx::GPX* root;

public:
	explicit GPX(const std::string& path);
	~GPX();

	Cache get_cache(const std::string& code) const;
	Caches get_user_caches(const std::string& uuid = "", int count = 0) const override;
};
