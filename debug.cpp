#include "debug.h"

// #include <ctime>

int Debug::debug_level = 1;

Debug::Debug(int n) : lvl(n) {
	if (lvl <= debug_level) {
		std::string indent;

		indent.append(lvl - 1, '\t');
		if (lvl > 1) indent.append("*** ");
		buffer << indent;

		// std::time_t now = std::time(nullptr);
		// std::string dt = std::ctime(&now);
		// std::cout << dt << " ";
	}
}

Debug::~Debug() {
	if (lvl <= debug_level) std::cerr << buffer.str() << '\n';
}

void Debug::set_debug_level(int n) { debug_level = n; }
