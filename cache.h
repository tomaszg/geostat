#pragma once

#include <string>
#include <iostream>
#include <ctime>

#include "common.h"

// enum Service {
// 	gc,
// 	ocpl,
// 	ocde,
// 	ocna,
// 	ocro,
// 	ocuk,
// 	ocnl,
// 	gcsu
// };

// enum Type {
// 	traditional,
//     multi,
//     quiz, // also known as "Mystery"
//     moving, // a geocache with changing coordinates
//     virt,
//     webcam,
//     other, // also dubbed "unknown type"; allows OC users to create special caches which don't fit into the scheme of well-known types
//     event, // a peculiar type of geocache which is NOT a geocache at all, but it is stored as a geocache in OC database. Just keep in mind, that in case of Event Caches, some fields may have a little different meaning than you would tell by their name
//     own, // a moving geocache which is carried by the owner
//     podcast //a geocache with attached MP3 file(s). The MP3 data is not accessible via OKAPI. This type is only in use at Opencaching.US
// };

enum Status {
	ok, // available
	disabled, // temporarily disabled
	archived, // archived
	unknown
};

class Cache {
public:
	std::string code;
	uint internal_id;
	Position pos;
	std::string name;
	Status status;
	std::string size;
	float diff = 0;
	float terr = 0;
	uint fav = 0;
	uint founds = 0;
	float rating = 0;
	bool recommended = 0; // was the cache recommended by that user?
	std::string type;
	std::string country;
	std::string region;
	std::string subregion;
	std::string origin;
	std::string owner;
	std::string owner_uuid;
	std::time_t date_t = 0;
	std::time_t date_hidden_t = 0;
	std::tm date_tm;
	std::tm date_hidden_tm;
	std::string year_month;
	std::string year;
	std::string mon;
	std::string day;
	std::string hour;
	std::string day_of_week;
	std::string date;
	std::string date_hidden;
	int age_when_found = -1;
	int age_now = -1;
	uint trail = 0;
	bool ftf = 0;

	void set_date(const std::tm& t);
	void set_date_hidden(const std::tm& t);

	static Position home;

	void show() const;
	std::string link() const;
	std::string link_name() const;
	std::string safe_name() const;
	// float distance() const;
};
