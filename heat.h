#pragma once

#include "cache.h"
#include "maps.h"

#include <set>
#include <string>

class Heat {
private:
	const Map* mp;

public:
	explicit Heat(const Map* m);

	void generate(const std::string& filename, const pCaches& points, int stamp_size, const std::string& theme = "soft");
	void generate_path(const std::string& filename, const Date_Caches& sorted);
	void generate_anim(const std::string& filename, const Date_Caches& sorted, int dot_size);
};
