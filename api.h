#pragma once

#include "cache.h"

#include <vector>

class Api {
public:
	Api() {}
	virtual ~Api() {}

	// virtual Cache get_cache(std::string code) = 0;
	// virtual Caches get_caches(std::vector<std::string> codes) = 0;
	virtual Caches get_user_caches(const std::string& uuid, int count = 0) const = 0;
};
